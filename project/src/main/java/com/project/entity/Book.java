package com.project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "books")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Pattern(regexp = "\\D+", message = "Book Name should not contain numbers")
    @Size(min = 3,max = 50,message = "Book Name length should be in between 3 to 50")
    private String bookName;

    @Pattern(regexp = "\\D+",message = "Author Name should not contain numbers")
    @Size(min = 3,max = 50,message = "Author Name length should be in between 3 to 50")
    private String authorName;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date publishDate;

    @NotNull
    private long price;
}
