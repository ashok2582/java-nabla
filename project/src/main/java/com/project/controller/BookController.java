package com.project.controller;

import com.project.entity.Book;
import com.project.service.BookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@Slf4j
public class BookController {

    @Autowired
    private BookService service;

    @Autowired
    private RabbitTemplate template;

//    @PostMapping("/publish")
//    public String publishData(@RequestParam("exchangeName") String exchangeName,
//                              @RequestParam("routingKey") String routingKey,
//                              @RequestBody Book book){
////        student.setId(Integer.valueOf(UUID.randomUUID().toString()));
//
//        log.info("Inside publishData Method of StudentController");
//        return "Record Sends Successfully";
//    }

    @GetMapping("/")
    public String viewHomePage(Model model){
        model.addAttribute("listBooks",service.getAllBooks());
        log.info("Inside viewHomePage method of BookController class");
        return "index";
    }

    @GetMapping("/showNewBookForm")
    public String showNewBookForm(Model model){
        Book book = new Book();
        model.addAttribute("book",book);
        log.info("Inside showNewBookForm method of BookController class");
        return "new_book";
    }

    @PostMapping("/saveBook")
    public String saveBook(@Valid @ModelAttribute("book")Book book, BindingResult result){
        if(result.hasErrors()){
            return "new_book";
        }
        template.convertAndSend("topic-exchange","queue.admin",book);
        service.saveBook(book);
        log.info("Inside saveBook method of BookController class");
        return "redirect:/";
    }

    @GetMapping("/showFormForUpdate/{id}")
    public String showFormForUpdate(@PathVariable("id")long id, Model model){
        Optional<Book> book = service.getBookById(id);
        model.addAttribute("book",book);
        log.info("Inside showFormForUpdate method of BookController class");
        return "update_book";
    }

    @GetMapping("/deleteBook/{id}")
    public String deleteBook(@PathVariable("id") long id){
        this.service.deleteBook(id);
        log.info("Inside deleteBook method of BookController class");
        return "redirect:/";
    }
}
