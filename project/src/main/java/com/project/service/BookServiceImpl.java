package com.project.service;

import com.project.entity.Book;
import com.project.repository.BookRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class BookServiceImpl implements BookService{

    @Autowired
    private BookRepository repository;

    @Override
    public List<Book> getAllBooks() {
        log.info("Inside getAllBooks method of BookServiceImpl class");
        return repository.findAll();
    }

    @Override
    public Book saveBook(Book book) {
        log.info("Inside saveBook method of BookServiceImpl class");
        return repository.save(book);
    }

    @Override
    public Optional<Book> getBookById(long id) {
        log.info("Inside getBookById method of BookServiceImpl class");
        return repository.findById(id);
    }

    @Override
    public void deleteBook(long id) {
        log.info("Inside deleteBook method of BookServiceImpl class");
        repository.deleteById(id);
    }
}
