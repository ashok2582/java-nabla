package com.project.service;

import com.project.entity.Book;

import java.util.List;
import java.util.Optional;

public interface BookService {
    List<Book> getAllBooks();

    Book saveBook(Book book);

    Optional<Book> getBookById(long id);

    void deleteBook(long id);
}
