package com.consumer.consumer;

import com.consumer.configuration.RabbitMQConfiguration;
import com.consumer.entity.Book;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class BookConsumer {

    @RabbitListener(queues = "allQueue")
    public void listener(Book book){
        System.out.println(book);
    }
}
