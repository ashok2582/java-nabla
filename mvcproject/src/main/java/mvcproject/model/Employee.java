package mvcproject.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotBlank(message = "Employee name can not be empty!!")
	private String name;
	@Email
	private String email;
	@NotBlank(message = "city can not be empty!!")
	private String city;
    
//	public static String validateEmail(String email) {
//		if (email == null || email.isEmpty()) {
//			return "Invalid";
//		}
//		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
//		"[a-zA-Z0-9_+&*-]+)*@"+
//	    "(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
//		
//		Pattern pattern = Pattern.compile(emailRegex);
//		if(pattern.matcher(email).matches()){
//			return "Valid";
//		}
//		else{
//			return "Invalid";
//		}
//	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Employee(int id, String name, String email, String city) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.city = city;
	}

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", email=" + email + ", city=" + city + "]";
	}

}
