package mvcproject.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

import mvcproject.dao.EmployeeDao;
import mvcproject.model.Employee;


@Controller
public class MainController {
	
	@Autowired
	private EmployeeDao employeeDao;

	@RequestMapping("/")
	public String home(Model model)
	{
		List<Employee> employees = employeeDao.getAllEmployee();
		model.addAttribute("employees",employees);
		return "index";
	}
	
	@RequestMapping("/add")
	public String createEmployee(@Valid Model model)
	{
		model.addAttribute("title","Add Employee");
		return "add_Employee";
	}
	
	@RequestMapping(value="/handle-employee",method=RequestMethod.POST)
	public RedirectView handleEmployee(@Valid @ModelAttribute Employee employee,BindingResult result ,HttpServletRequest request)
	{
		if(result.hasErrors()){
			System.out.println(result);
		}
		System.out.println(employee);
		employeeDao.createEmployee(employee);
		RedirectView redirectView = new RedirectView();
		redirectView.setUrl(request.getContextPath()+"/");
		return redirectView;
	}
	
	@RequestMapping("/delete/{id}")
	public RedirectView deleteEmployee(@PathVariable("id") int id,HttpServletRequest request){
		this.employeeDao.deleteEmployee(id);
		RedirectView redirectView = new RedirectView();
		redirectView.setUrl(request.getContextPath()+"/");
		return redirectView;
	}
	
	@RequestMapping("/update/{id}")
	public String updateEmployee(@PathVariable("id") int id,Model model){
		Employee employee = this.employeeDao.getEmployee(id);
		model.addAttribute("employee",employee);
		return "update_Employee";
	}
	
}
