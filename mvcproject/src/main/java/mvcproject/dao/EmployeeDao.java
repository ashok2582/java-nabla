package mvcproject.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;

import mvcproject.model.Employee;

@Component
public class EmployeeDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	@Transactional
	public void createEmployee(Employee employee){
		
		this.hibernateTemplate.saveOrUpdate(employee);
	}
	
	public List<Employee> getAllEmployee(){
		List<Employee> employee = this.hibernateTemplate.loadAll(Employee.class);
		return employee;
	}
	
	@Transactional
	public void deleteEmployee(int id){
		Employee e = this.hibernateTemplate.load(Employee.class, id);
		this.hibernateTemplate.delete(e);
	}
	
	public Employee getEmployee(int id){
		return this.hibernateTemplate.get(Employee.class, id);
	}
	
}
