<html>
<head>
<%@include file="./base.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>
	<div class="container mt-3">
		<div class="row">
			<div class="col-md-12">

				<h1 class="text-center mb-3">Welcome to Employee App</h1>
				<table class="table table-bordered">
					<thead style="background-color : tomato">
					 <tr>
					   <th scope="col">ID</th>
					   <th scope="col">Name</th>
					   <th scope="col">Email</th>
					   <th scope="col">City</th>
					   <th scope="col">Actions</th>
					 </tr>
					</thead>
					<tbody>
					
					  <c:forEach items="${employees }" var="e">
					  <tr>
					     <th scope="row">NIPL${e.id }</th>
					     <th scope="row">${e.name }</th>
					     <th scope="row">${e.email }</th>
					     <th scope="row">${e.city }</th>
					     <td>
					       <a href="delete/${e.id }"><button class="btn btn-outline-danger">Delete</button></a>
					       <a href="update/${e.id }"><button class="btn btn-outline-info">Update</button></a>
					     </td>
					  </tr>
					  </c:forEach>
					  
					</tbody>
				</table>
				<div class="container text-center">
				
				  <a href="add" class="btn btn-outline-success">Add Employee</a>
				
				</div>
			</div>
		</div>
	</div>
</body>
</html>
