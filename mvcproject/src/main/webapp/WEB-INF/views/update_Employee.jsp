<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="./base.jsp"%>
</head>
<body>

	<div class="container mt-3">

		<div class="row">

			<div class="col-md-6 offset-md-3">

				<h1 class="text-center mb-3">Update Employee Details</h1>

				<form action="${pageContext.request.contextPath }/handle-employee"
					method="post">

					<input type="hidden" value="${employee.id }" name="id">

					<div class="form-group">
						<lable for="name">Employee Name</lable>
						<input type="text" class="form-control" id="name"
							aria-describedby="emailHelp" name="name"
							placeholder="Enter Name here" value="${employee.name }">
					</div>

					<div class="form-group">
						<lable for="email">Employee Email</lable>
						<input type="email" class="form-control" id="email"
							aria-describedby="emailHelp" name="email"
							placeholder="Enter Email here" value="${employee.email }">
					</div>

					<div class="form-group">
						<lable for="city">Employee City</lable>
						<input type="text" class="form-control" id="city"
							aria-describedby="emailHelp" name="city"
							placeholder="Enter City here" value="${employee.city }">
					</div>

					<div class="container text-center">
						<a href="${pageContext.request.contextPath }/"
							class="btn btn-outline-danger">Back</a>
						<button type="submit" class="btn btn-warning">Update</button>
					</div>

				</form>

			</div>

		</div>

	</div>

</body>
</html>