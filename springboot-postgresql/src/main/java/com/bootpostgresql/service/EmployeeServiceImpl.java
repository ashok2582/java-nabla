package com.bootpostgresql.service;

import com.bootpostgresql.entity.Employee;
import com.bootpostgresql.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    private EmployeeRepository repository;

    @Override
    public List<Employee> getAllEmployees() {
        return repository.findAll();
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        return repository.save(employee);
    }

    @Override
    public Employee getEmployeeById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public void deleteEmployeeById(long id) {
        this.repository.deleteById(id);
    }
}
