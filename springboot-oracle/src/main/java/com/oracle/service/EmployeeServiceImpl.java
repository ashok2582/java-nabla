package com.oracle.service;

import com.oracle.entity.Employee;
import com.oracle.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    @Override
    public Employee saveEmployee(Employee employee) {
        return repository.save(employee);
    }

    @Override
    public Employee getEmployeeById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public void deleteEmployeeById(long id) {
        repository.deleteById(id);
    }

    @Override
    public List<Employee> getAllEmployees() {
        return repository.findAll();
    }
}
