package com.oracle.service;

import com.oracle.entity.Employee;

import java.util.List;

public interface EmployeeService {
    Employee saveEmployee(Employee employee);

    Employee getEmployeeById(long id);

    void deleteEmployeeById(long id);

    List<Employee> getAllEmployees();
}
